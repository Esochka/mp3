<%@ page import="java.util.List" %>
<%@ page import="com.devEducation.model.Song" %>
<%@ page import="com.devEducation.model.SongDto" %>
<%@ page import="java.util.HashSet" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Main</title>
</head>
<body>

<form  action="read" >
    <input type="submit" value="read">
</form>

<form action="create" >
    <input type="text" name="name">
    <input type="text" name="genre">
    <input type="text" name="artist">
    <input type="text" name="album">
    <input type="text" name="link">
    <input type="text" name="time">
    <input type="year" name="year">
    </br>
    <input type="submit" value="create">
</form>

<form action="update" >
    <input type="text" name="id">
    <input type="text" name="name">
    <input type="text" name="genre">
    <input type="text" name="artist">
    <input type="text" name="album">
    <input type="text" name="link">
    <input type="text" name="time">
    <input type="year" name="year">
    </br>
    <input type="submit" value="update">
</form>

<form action="delete" >

    <input type="text" name="id">
    </br>
    <input type="submit" value="delete">
</form>
</br>
<table>
    <%
        HashSet<SongDto> names = (HashSet<SongDto>) request.getAttribute("songs");

        if (names != null && !names.isEmpty()) {
            for (SongDto s : names) {
                out.println("<tr><td>" + s.getId() + "</td><td>" + s.getName() + "</td><td>" + s.getGenre() + "</td><td>" + s.getArtist() + "</td><td>" + s.getAlbum() + "</td><td>" + s.getLink() + "</td><td>" + s.getTime() + "</td></tr>");
            }
        }


    %>


</table>

</body>
</html>
