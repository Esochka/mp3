package com.devEducation.servlet;

import com.devEducation.config.Gonfig;
import com.devEducation.json.GetJson;
import com.devEducation.model.Artist;
import com.devEducation.model.Genre;
import com.google.gson.Gson;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@WebServlet(name = "ArtistsServlet", urlPatterns = "/getArtists")
public class ArtistsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String genre = request.getParameter("genre");

        EntityManagerFactory entityManagerFactory = Gonfig.getEntityManager();
        EntityManager entityManager2 = entityManagerFactory.createEntityManager();
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");


            entityManager2.getTransaction().begin();
            List<Artist> result2 = entityManager2.createQuery("SELECT a FROM Artist a ", Artist.class).getResultList();
            Set<Artist> artistList = new HashSet<>();
            entityManager2.getTransaction().commit();
            System.out.println(result2);

            for (Artist a: result2) {
                List<Genre> genres = a.getGenre();
                for (Genre genres2: genres) {
                    if (genres2.getGenre().equals(genre)){
                        artistList.add(a);
                    }
                }
            }
            System.out.println(result2);


        String stringList = new String();

        for (Artist artist: artistList) {
            stringList += (artist.getName())+",";
        }


        out.print(new Gson().toJson(stringList));
        out.flush();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        doGet(request, response);
    }
}


