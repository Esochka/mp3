package com.devEducation.servlet;



import com.devEducation.config.Gonfig;
import com.devEducation.model.Song;
import com.devEducation.model.SongDto;
import com.google.gson.Gson;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@WebServlet(name = "ArtistsServlet4", urlPatterns = "/read")
public class Re extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {





        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        EntityManagerFactory entityManagerFactory = Gonfig.getEntityManager();
        EntityManager entityManager2 = entityManagerFactory.createEntityManager();
        PrintWriter out = resp.getWriter();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");


        entityManager2.getTransaction().begin();
        List<Song> result4 = entityManager2.createQuery("SELECT a FROM Song a ", Song.class).getResultList();

        Set<SongDto> songDtos = new HashSet<>();
        for (Song a : result4) {

                songDtos.add(new SongDto(a.getId(), a.getName(), a.getGenre().getGenre(), a.getArtist().getName(), a.getLink(), a.getTime(), a.getAlbum().getAlbum(), a.getAlbum().getYear()));

            }


        req.setAttribute("songs", songDtos);



        RequestDispatcher requestDispatcher = req.getRequestDispatcher("start.jsp");
        System.out.println("sdsdsd");
        requestDispatcher.forward(req, resp);
    }



    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doGet(request, response);
    }



}


