package com.devEducation.servlet;

import com.devEducation.config.Gonfig;
import com.devEducation.json.GetJson;
import com.devEducation.model.Artist;
import com.devEducation.model.Genre;
import com.devEducation.model.Song;
import com.devEducation.model.SongDto;
import com.google.gson.Gson;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@WebServlet(name = "ArtistsServlet2", urlPatterns = "/getSong")
public class SongServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String genre = request.getParameter("genre");
        String artist = request.getParameter("artist");

        EntityManagerFactory entityManagerFactory = Gonfig.getEntityManager();
        EntityManager entityManager2 = entityManagerFactory.createEntityManager();
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");


        entityManager2.getTransaction().begin();
        List<Song> result4 = entityManager2.createQuery("SELECT a FROM Song a ", Song.class).getResultList();
        List<Song> artistList = new ArrayList<>();
        entityManager2.getTransaction().commit();
        System.out.println(result4);
        List<Song> songList = new ArrayList<>();


        Set<SongDto> songDtos = new HashSet<>();
        for (Song a : result4) {
            if (a.getGenre().getGenre().equals(genre) && a.getArtist().getName().equals(artist)) {
                songDtos.add(new SongDto(a.getName(), a.getGenre().getGenre(), a.getArtist().getName(), a.getLink(), a.getTime(), a.getAlbum().getAlbum(), a.getAlbum().getYear()));
            }
        }

        out.print(new Gson().toJson(songDtos));
        out.flush();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        doGet(request, response);
    }
}


