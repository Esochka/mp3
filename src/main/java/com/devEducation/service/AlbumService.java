package com.devEducation.service;

import com.devEducation.config.Gonfig;
import com.devEducation.model.Album;

import javax.persistence.EntityManager;

public class AlbumService {
    private static EntityManager entityManager;

    static {

            entityManager = Gonfig.getEntityManager().createEntityManager();

    }

    public static void save(Album album) {
        entityManager.getTransaction().begin();
        entityManager.persist(album);
        entityManager.getTransaction().commit();
    }
}
