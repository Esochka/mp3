package com.devEducation.service;

import com.devEducation.config.Gonfig;
import com.devEducation.model.Artist;
import com.devEducation.model.Song;

import javax.persistence.EntityManager;

public class SongService {

    private static EntityManager entityManager;

    static {

            entityManager = Gonfig.getEntityManager().createEntityManager();

    }

    public static void save(Song song) {
        entityManager.getTransaction().begin();
        entityManager.persist(song);
        entityManager.getTransaction().commit();
    }
}
