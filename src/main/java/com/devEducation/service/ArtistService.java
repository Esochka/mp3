package com.devEducation.service;

import com.devEducation.config.Gonfig;
import com.devEducation.model.Album;
import com.devEducation.model.Artist;

import javax.persistence.EntityManager;

public class ArtistService {

    private static EntityManager entityManager;

    static {

            entityManager = Gonfig.getEntityManager().createEntityManager();

    }

    public static void save(Artist artist) {
        entityManager.getTransaction().begin();
        entityManager.persist(artist);
        entityManager.getTransaction().commit();
    }
}
