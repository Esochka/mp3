package com.devEducation.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Song {
    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    @ManyToOne
    @JoinColumn(name="id_genge")
    private Genre genre;

    @ManyToOne
    @JoinColumn(name="id_artist")
    private Artist artist;

    @ManyToOne
    @JoinColumn(name="id_album")
    private Album album;

    private String link;

    private String time;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Song song = (Song) o;
        return Objects.equals(name, song.name) &&
                Objects.equals(genre, song.genre) &&
                Objects.equals(artist, song.artist) &&
                Objects.equals(album, song.album) &&
                Objects.equals(link, song.link) &&
                Objects.equals(time, song.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, genre, artist, album, link, time);
    }

    public Song() {
    }

    public Song(String name, Genre genre, Artist artist, Album album, String link, String time) {
        this.name = name;
        this.genre = genre;
        this.artist = artist;
        this.album = album;
        this.link = link;
        this.time = time;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Song{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", genre=").append(genre);
        sb.append(", artist=").append(artist);
        sb.append(", album=").append(album);
        sb.append(", link='").append(link).append('\'');
        sb.append(", time='").append(time).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
