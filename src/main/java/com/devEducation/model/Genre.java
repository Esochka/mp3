package com.devEducation.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Genre")
public class Genre {
@Id
@GeneratedValue
    private Integer id;

    private String name;

    @ManyToOne
    @JoinColumn(name="Artist_id")
    private Artist artists;

    @OneToMany
    List<Song> songs = new ArrayList<>();

    public Genre(String name) {
        this.name = name;
    }

    public String getGenre() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Genre genre = (Genre) o;
        return Objects.equals(name, genre.name) &&
                Objects.equals(artists, genre.artists);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, artists);
    }

    public Genre() {
    }

    public void setGenre(String name) {
        this.name = name;
    }

    public void setArtists(Artist artists) {
        this.artists=artists;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Genre{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
