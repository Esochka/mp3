package dao;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import model.Artist;
import model.Genre;
import model.Song;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.String.valueOf;

public class SongDao {

    public static List<String> setLink(String path) {
        List<String> genres = new ArrayList<>();
        try {
            String url = "http://localhost:8080/sample_servlet_war_exploded/addSong?url="+path;

            System.out.println(url);
            Jsoup.connect(url).ignoreContentType(true).get();

            Document doc = Jsoup.connect(url).ignoreContentType(true).get();
            System.out.println(doc.text());
            genres = Arrays.asList(new Gson().fromJson(valueOf(doc.text()), String.class).split(","));


        } catch (Exception ex) {
            System.err.println(ex);

        }
        return genres;
    }


    public static List<String> getArtistsByGenre(String genre) {
        List<String> artists = new ArrayList<>();
        try {
            String url = "http://localhost:8080/sample_servlet_war_exploded/getArtists?genre="+genre;
            System.out.println(url);
            Document doc = Jsoup.connect(url).ignoreContentType(true).get();
            System.out.println(doc.text());
            artists = Arrays.asList(new Gson().fromJson(valueOf(doc.text()), String.class).split(","));

        } catch (Exception ex) {
            System.err.println(ex);

        }
            return artists;

    }
    public static List<Song> getSongByArtist(String genre, String artist) {
        List<Song> songs= new ArrayList<>();
        try {
            String url = "http://localhost:8080/sample_servlet_war_exploded/getSong?genre="+genre+"&artist="+artist;
            System.out.println(url);
            Document doc = Jsoup.connect(url).ignoreContentType(true).get();
            System.out.println(doc.text());
            songs = new Gson().fromJson(String.valueOf(doc.text()),
                    new TypeToken<List<Song>>(){}.getType());
        } catch (Exception ex) {
            System.err.println(ex);

        }

        return songs;
    }
}
